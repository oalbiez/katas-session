package katas.foobar;

import java.util.LinkedHashMap;
import java.util.Map;

public class Foobar {
    //TODO: la map cache trop le problème de l'ordre
    // Option 1: DSL
    // "rules=list(
    //      whenDivisibleBy(3).then("Foo"),
    //      whenDivisibleBy(5).then("Bar"));"
    // Option 2: Enum

    private static Map<Integer, String> configuration = createConfiguration();


    static String foobar(int input) {
        final StringBuilder retourProvisoir = new StringBuilder();
        for (Map.Entry<Integer, String> entry : configuration.entrySet()) {
            if (isDivisibleBy(input, entry.getKey()))
                retourProvisoir.append(entry.getValue());
        }
        if (retourProvisoir.length() == 0) {
            return Integer.toString(input);
        }
        return retourProvisoir.toString();
    }

    private static Map<Integer, String> createConfiguration() {

        Map<Integer, String> configuration = new LinkedHashMap<>();
        configuration.put(3, "Foo");
        configuration.put(5, "Bar");
        return configuration;
    }


    private static boolean isDivisibleBy(int value, int divisor) {
        return value % divisor == 0;
    }
}
