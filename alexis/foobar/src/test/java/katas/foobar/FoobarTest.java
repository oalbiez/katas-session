package katas.foobar;

import org.junit.Test;

import static katas.foobar.Foobar.foobar;
import static org.junit.Assert.assertEquals;

public class FoobarTest {

    @Test
    public void should_return_foo_when_input_is_divisble_only_by_3() {
        assertEquals("Foo", foobar(3));
        assertEquals("Foo", foobar(6));
    }

    @Test
    public void should_return_bar_when_input_is_divisble_only_by_5() {
        assertEquals("Bar", foobar(5));
        assertEquals("Bar", foobar(25));
    }

    @Test
    public void should_return_input_when_input_is_not_divisible_by_3_nor_5() {
        assertEquals("2", foobar(2));
        assertEquals("7", foobar(7));
    }

    @Test
    public void should_retuen_FooBar_when_input_is_divisible_by_3_and_5() {
        assertEquals("FooBar",foobar(15));
    }
}
