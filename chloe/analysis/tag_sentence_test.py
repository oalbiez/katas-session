import spacy
from pytest import mark


def is_masculin(word):
    return word in ["homme", "savoir"]


class analisys:
    def __init__(self):
        self.tags = {}

    def tag(self, sent, tag):
        self.tags[sent] = tag

    def get_tag_for(self, sent):
        if sent in self.tags:
            return self.tags[sent]
        return "neutre"


class parser:
    def __init__(self):
        self.__nlp = spacy.load("fr_core_news_sm")

    def analyse(self, text):
        doc = self.__nlp(text)
        result = analisys()
        for sent in doc.sents:
            for word in sent:
                if is_masculin(word.text):
                    result.tag(sent, "masculin")
        return (doc, result)




# class analysed_sent:
#     def __init__(self, sent):
#         self.__sent = sent
#         self.__is_masculin = False
#
#     def sent(self):
#         return self.__sent
#
#     def set_masculin(self):
#         self.__is_masculin = True
#
#     def is_masculin(self):
#         return self.__is_masculin
#
#
# def analyse2(text):
#     doc = nlp(text)
#     result = []
#     for sent in doc.sents:
#         s = analysed_sent(sent)
#         result.append(s)
#         for word in sent:
#             if word.text in ["homme", "savoir"]:
#                 s.set_masculin()
#
#     return result


def test_1():
    assert is_masculin("homme") == True


@mark.slow
def test_():
    doc, result = parser().analyse("La règle que suit en cela un homme sage.")
    for sent in doc.sents:
        assert result.get_tag_for(sent) == "masculin"

@mark.slow
def test__():
    doc, result = parser().analyse("C’est de renoncer à de légères voluptés.")
    for sent in doc.sents:
        assert result.get_tag_for(sent) == "neutre"


# def test__():
#     sents = analyse2("La règle que suit en cela un homme sage. C’est de renoncer à de légères voluptés. En avoir de plus grandes, et de savoir supporter des douleurs légères pour en éviter de plus fâcheuses.")
#     for sent in sents:
#         print("the sent " + repr(sent.sent().text) + ": " + repr(sent.is_masculin()))
#     assert False
