import unittest
from morpion import *

class MorpionTestCase(unittest.TestCase):
	def test01(self):
		self.checkPoints("x",
			"   ",
			"   ",
			"   ",
			(
				3, 2, 3,
				2, 4, 2,
				3, 2, 3,
			),
		)

	def test02(self):
		self.checkPoints("x",
			"   ",
			"o  ",
			"   ",
			(
				2, 2, 3,
				0, 3, 1,
				2, 2, 3,
			),
		)

	def test03(self):
		self.checkPoints("o",
			"xx ",
			"o  ",
			"   ",
			(
				0, 0, 2,
				0, 2, 2,
				2, 1, 2,
			),
		)

	def test04(self):
		self.checkLinesOpportunities("x",
			"x  ",
			"o  ",
			"   ",
			(
				0, 1, 1,
				0, 1, 0,
				0, 0, 1,
			),
		)

	def checkPoints(self, joueur, line1, line2, line3, expPoints):
		points = givePoint(makeboard(
			line1,
			line2,
			line3,),
			joueur,
		)
		self.failUnlessEqual(
			expPoints,
			points,
		)



if __name__ == "__main__":
	unittest.main()


