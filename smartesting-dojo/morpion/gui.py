#!/usr/bin/env python
# -*- encoding: latin-1 -*-
"""A program for demonstrating the graphical abilities of Python.
It uses just the 2D mode, who needs 3D?
Some demos are taken from XScreenSaver (really cool prog)
and simply ported to Python (without looking in the code)

Requirements:
- Python (written with 2.4)
- pygame (written with 1.6)

Written by Marek Kubica.
Dedicated to Fritz Cizmarov.
"""
import os, sys, random, time, math, optparse
import pygame
import pygame.locals as pyl

__version__ = '0.1.9'

class Gui(object):
	screenwidth = 680
	screenheight = 480
	screensize = (screenwidth, screenheight)
	clamp_window = pyl.Rect(0, 0, screenwidth, screenheight)
	fps = 60

	def __init__(self):
		"""Internal: Prepare the system for running demos"""
		# initialise pygame
		pygame.init()

		# open a window
		self.screen = pygame.display.set_mode(self.screensize)
		# set the window title
		pygame.display.set_caption('Screensaw ' + __version__)
		# hide mouse
		pygame.mouse.set_visible(False)

		# create the surface
		self.background = pygame.Surface(self.screen.get_size())
		# convert it to be faster
		self.background = self.background.convert()
		# fill it black
		self.background.fill((0, 0, 0))
		# create a clock to control the FPS
		self.clock = pygame.time.Clock()
		self.scale = 100
		self.offset = 100


	def drawMorpionGrid(self):
		y1 = 0
		y2 = self.scale * 3
		for x in xrange(1,3):
			x1 = x * self.scale
			pygame.draw.line(self.background, (0, 255, 255), (x1+self.offset, y1+self.offset), (x1+self.offset, y2+self.offset), 2)
		x1 = 0
		x2 = self.scale * 3
		for y in xrange(1,3):
			y1 = y * self.scale
			pygame.draw.line(self.background, (0, 255, 255), (x1+self.offset, y1+self.offset), (x2+self.offset, y1+self.offset), 2)


	def getBoxCenter(self, pos):
		x = pos % 3
		y = pos / 3
		offset = self.offset + self.scale / 2
		return (x * self.scale + offset, y * self.scale + offset)


	def drawO(self, pos):
		pygame.draw.circle(self.background, (255, 255, 255), self.getBoxCenter(pos), self.scale/3, 12)

	def drawX(self, pos):
		centerX, centerY =self.getBoxCenter(pos)
		offset = self.scale /4
		pygame.draw.line(self.background, (255, 155, 255), (centerX-offset, centerY-offset), (centerX+offset, centerY+offset), 12)
		pygame.draw.line(self.background, (255, 155, 255), (centerX+offset, centerY-offset), (centerX-offset, centerY+offset), 12)


	def display(self):
		"""Displays the stuff"""
		self.screen.blit(self.background, (0, 0))
		pygame.display.flip()

	def drawBoard(self, board):
		for index, value in enumerate(board):
			if value == "x":
				self.drawX(index)
			if value == "o":
				self.drawO(index)


import time
import morpion

def main():
	gui = Gui()
	gui.drawMorpionGrid()
	board = [" "] * 9
	gui.drawBoard(board)
	gui.display()
	for x in xrange(4):
		gui.drawBoard(morpion.jouer(board, "x"))
		status, winningLine = morpion.internalStatus(board)

		gui.display()
		time.sleep(1)
		gui.drawBoard(morpion.jouer(board, "o"))
		status, winningLine = morpion.internalStatus(board)
		gui.display()
		time.sleep(1)
	time.sleep(3)



if __name__ == '__main__':
	main()