inProgress = 0
oWon = 1
xWon = -1
draw = 2

suites = (
	(0,1,2),
	(3,4,5),
	(6,7,8),
	(0,3,6),
	(1,4,7),
	(2,5,8),
	(0,4,8),
	(2,4,6)
)

def makeboard(line1, line2, line3):
	return list(line1) + list(line2) + list(line3)


def givePoint(board, joueur):
	points =[]
	opponent = other(joueur)
	for case in xrange(9):
		point = 0
		for suite in suites:
			line = "".join(board[index] for index in suite)
			if case in suite and not opponent in line and board[case] == " ":
				point += 1
		points.append(point)
	return tuple(points)


def other(entity):
		return {
			"o": "x",
			"x": "o",
			oWon: xWon,
			xWon: oWon,
		}[entity]


def jouer(board, joueur):
	def findWinningPosition(player, wantedResult):
		for index, symbol in enumerate(board):
			if symbol == " ":
				newBoard = board[:]
				newBoard[index] = player
				if status(newBoard) == wantedResult:
					return index
		return None


	winningResult = {
		"o": oWon,
		"x": xWon,
	}[joueur]

	winningPosition = findWinningPosition(joueur, winningResult)
	if winningPosition is not None:
		board[winningPosition] = joueur
		return board

	blockingResult = other(winningResult)
	blockingPosition = findWinningPosition(other(joueur), blockingResult)
	if blockingPosition is not None:
		board[blockingPosition] = joueur
		return board




	index = board.index(" ")
	board[index] = joueur
	return board

def status(board):
	return 	internalStatus(board)[0]


def internalStatus(board):

	winningLines = {
		"ooo": oWon,
		"xxx": xWon,
	}

	for suite in suites:
		line = "".join(board[index] for index in suite)
		winner = winningLines.get(line)
		if winner is not None:
			return winner, (suite[0], suite[2])

	if " " in board :
		return inProgress, (None, None)

	return draw, (None, None)


