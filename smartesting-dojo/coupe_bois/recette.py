from couper import calcule_coupe


import unittest


class CoupeRecette(unittest.TestCase):

	def test_01(self):
		""" Planches de 300 cm; couper 10x20 cm, 4x10 cm, 4x15cm
			Resultat: 1 planche nécesaire,
			Détail ([20=>10, 10=>4, 15=>4], chute=0)
		"""
		planches = calcule_coupe(taille=300, elements=((20,10), (10,4), (15,4)))
		self.failUnlessEqual(planches, (
			((20,10),(10,4),(15,4)),
		))

	def test_02(self):
		""" Planches de 250 cm; couper 10x20 cm, 4x10 cm, 4x15cm
			Resultat: 2 planches nécesaires,
			1. ([20=>10, 10=>2, 15=>2], chute=0),
			2. ([10=>2, 15=>2], chute=200)
		"""
		planches = calcule_coupe(taille=250, elements=((20,10), (10,4), (15,4)))
		self.failUnlessEqual(planches, (
			((20,10),(10,2),(15,2)),
			((10,2),(15,2)),
		))

	def test_03(self):
		""" Planches de 250 cm; couper 5x70 cm, 7x15cm
			Resultat: 2 planches nécesaires,
			1. ([15=>7, 70=>2], chute=5),
			2. ([70=>3], chute=40)
		"""
		planches = calcule_coupe(taille=250, elements=((70,5), (15,7)))
		self.failUnlessEqual(planches, (
			((15,7),(70,2)),
			((70,3)),
		))

	def test_04(self):
		""" Planches de 250 cm; couper 5x70 cm, 7x15 cm, 3x20 cm
			* Resultat: 3 planches nécesaires,
			1. ([70=>3, 20=>2, ], chute=0),
			2. ([70=>2], 20=>2, 15=>6] chute=0),
			3. ([15=>1], chute=235),
		"""
		planches = calcule_coupe(taille=250, elements=((70,5), (15,7), (20,3)))
		self.failUnlessEqual(planches, (
			((70,3),(20,2)),
			((70,2),(20,2), (15,6)),
			((15,1)),
		))



if __name__ == "__main__":
	unittest.main()


