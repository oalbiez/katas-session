import unittest

from couper import get_combinations


class TestCoupe(unittest.TestCase):

	def setUp(self):
		pass

	def test01(self):
		self.failUnlessEqual(
			get_combinations(range(1)),
			((0), )
		)


	def test02(self):
		self.failUnlessEqual(
			get_combinations(range(2)),
			((0,1), (1,0)),
		)


if __name__ == "__main__":
	unittest.main()


