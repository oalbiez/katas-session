import unittest

from converter import convert


class converter_testcase(unittest.TestCase):
	def test01(self):
		self.failUnlessEqual(convert("I"), 1)
		self.failUnlessEqual(convert("V"), 5)
		self.failUnlessEqual(convert("X"), 10)
		self.failUnlessEqual(convert("L"), 50)
		self.failUnlessEqual(convert("C"), 100)
		self.failUnlessEqual(convert("D"), 500)
		self.failUnlessEqual(convert("M"), 1000)


	def test02(self):
		self.failUnlessEqual(convert("II"), 2)
		self.failUnlessEqual(convert("LX"), 60)
		self.failUnlessEqual(convert("MC"), 1100)

	def test03(self):
		self.failUnlessEqual(convert("IV"), 4)

	def test04(self):
		self.failUnlessEqual(convert("MIV"),1004)
		self.failUnlessEqual(convert("MCMLVII"),1957)
		self.failUnlessEqual(convert("XMIV"),994)


if __name__ == "__main__":
	unittest.main()

