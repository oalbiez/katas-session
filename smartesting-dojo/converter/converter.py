

values = {
	"I": 1,
	"V": 5,
	"X": 10,
	"L": 50,
	"C": 100,
	"D": 500,
	"M": 1000,
}


def convert(roman):
	if len(roman) == 1:
		return values[roman]
	current, next = (values[x] for x in roman[:2])
	return convert(roman[1:]) + (current, -current)[next > current]


