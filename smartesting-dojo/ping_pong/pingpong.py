
class Ball:
	def __init__(self, initial_position=(0, 0), speed=(0, 0)):
		self.position = initial_position
		self.lastPosition = self.position
		self.speed = speed

	def nextPosition(self, land=None):
		lastSpeed = self.speed
		if land:
			land.visitBall(self)
		# Danger Hack below...
		if lastSpeed == self.speed:
			self.lastPosition = self.position
		self.position = (self.position[0]+self.speed[0], self.position[1]+self.speed[1])
		return self.position


class Land:
	def __init__(self, *borders):
		self.borders = borders

	def visitBall(self, ball):
		for each_border in self.borders:
			each_border.visitBall(ball)


def isWithin(value, a, b):
	return min(a, b) <= value <= max(a, b)

class VerticalBorder:
	def __init__(self, xPosition):
		self.xPosition = xPosition

	def visitBall(self, ball):
		lastSpeed = ball.position[0] - ball.lastPosition[0]
		print "lastSpeed ", lastSpeed
		print "ball.speed", ball.speed[0]
		if isWithin(self.xPosition, ball.lastPosition[0], ball.position[0]):
			ball.speed = (-ball.speed[0], ball.speed[1])
