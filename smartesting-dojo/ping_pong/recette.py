from pingpong import *

import unittest


class PingPongTest(unittest.TestCase):

	def test01(self):
		ball = Ball((0, 2), (1, 0))
		self.failUnlessEqual(ball.nextPosition(), (1, 2))
		self.failUnlessEqual(ball.nextPosition(), (2, 2))
		self.failUnlessEqual(ball.nextPosition(), (3, 2))

	def test02(self):
		ball = Ball((0, 2), (1, 0))
		border = VerticalBorder(3)
		land = Land(border)
		self.failUnlessEqual(ball.nextPosition(land), (1, 2))
		self.failUnlessEqual(ball.nextPosition(land), (2, 2))
		self.failUnlessEqual(ball.nextPosition(land), (3, 2))
		self.failUnlessEqual(ball.nextPosition(land), (2, 2))

	def test03(self):
		ball = Ball((3, 2), (-1, 0))
		border = VerticalBorder(0)
		land = Land(border)
		self.failUnlessEqual(ball.nextPosition(land), (2, 2))
		self.failUnlessEqual(ball.nextPosition(land), (1, 2))
		self.failUnlessEqual(ball.nextPosition(land), (0, 2))
		self.failUnlessEqual(ball.nextPosition(land), (1, 2))
		self.failUnlessEqual(ball.nextPosition(land), (2, 2))

	def test04(self):
		ball = Ball((3, 2), (-1, 0))
		land = Land(VerticalBorder(4))
		self.failUnlessEqual(ball.nextPosition(land), (2, 2))
		self.failUnlessEqual(ball.nextPosition(land), (1, 2))
		self.failUnlessEqual(ball.nextPosition(land), (0, 2))



	def test05(self):
		self.failUnless(isWithin(3, 2, 4))
		self.failUnless(isWithin(3, 4, 2))
		self.failIf(isWithin(1, 4, 2))
		self.failUnless(isWithin(3, 3, 4))
		self.failUnless(isWithin(0, 0, 0))


	def test06(self):
		ball = Ball((3, 2), (-1, 0))
		land = Land(
			VerticalBorder(0),
			VerticalBorder(4),
		)
		self.failUnlessEqual(ball.nextPosition(land), (2, 2))
		self.failUnlessEqual(ball.nextPosition(land), (1, 2))
		self.failUnlessEqual(ball.nextPosition(land), (0, 2))
		self.failUnlessEqual(ball.nextPosition(land), (1, 2))
		self.failUnlessEqual(ball.nextPosition(land), (2, 2))
		self.failUnlessEqual(ball.nextPosition(land), (3, 2))
		self.failUnlessEqual(ball.nextPosition(land), (4, 2))
		self.failUnlessEqual(ball.nextPosition(land), (3, 2))


if __name__ == "__main__":
	unittest.main()

