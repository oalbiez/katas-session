
class tile:
	black = "X"
	white = "O"


	def __init__(self, color):
		self._color = color


	def __str__(self):
		return self._color


	def is_black(self):
		return self._color == tile.black


	def is_white(self):
		return self._color == tile.white


	def set_black(self):
		self._color = tile.black


	def set_white(self):
		self._color = tile.white
