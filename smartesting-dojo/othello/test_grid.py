import unittest

from grid import grid


class grid_test(unittest.TestCase):
	def test01(self):
		g = grid()
		g.dump()
		self.failUnless(g.tile_at(3, 3).is_black())
		self.failUnless(g.tile_at(4, 4).is_black())
		self.failUnless(g.tile_at(3, 4).is_white())
		self.failUnless(g.tile_at(4, 3).is_white())


if __name__ == "__main__":
	unittest.main()

