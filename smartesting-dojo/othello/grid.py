from tile import tile


class grid:
	def __init__(self, width=8):
		self._grid = []
		for _ in range(width):
			row = []
			for _ in range(width):
				row.append(None)
			self._grid.append(row)

		self._grid[width / 2][width / 2] = tile(tile.black)
		self._grid[width / 2 - 1][width / 2 - 1] = tile(tile.black)
		self._grid[width / 2 - 1][width / 2] = tile(tile.white)
		self._grid[width / 2][width / 2 - 1] = tile(tile.white)


	def tile_at(self, column, row):
		return self._grid[column][row]


	def dump(self):
		for row in self._grid:
			for tile in row:
				if tile:
					print tile,
				else:
					print ".",
			print

