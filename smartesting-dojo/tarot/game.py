from card import Atout, Excuse


def required_suit(board):
	suit = board[0].suit
	if suit is Excuse:
		suit = board[1].suit

	return suit


def select_card(hand, board) :
	if not board:
		return hand[0]
	availableCards = select_suit(hand, board[0].suit)
	if availableCards:
		return availableCards[0]
	availableAtouts = select_suit(hand, Atout)
	if availableAtouts:
		return availableAtouts[0]
	return hand[0]


def select_suit(hand, suit):
	cards = []
	for card in hand:
		if isinstance(card, suit):
			cards.append(card)
	return tuple(cards)

