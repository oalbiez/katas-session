import unittest

from card import createCard
from card import Carreau, Trefle
from card import Un, Deux


class CardTestCase(unittest.TestCase):

	def test_00a(self):
		card= createCard(Carreau, Un)
		self.failUnlessEqual(repr(card), "Carreau/Un")


	def test_01a(self):
		" compare deux cartes identiques"
		card1 = createCard(Carreau, Un)
		card2 = createCard(Carreau, Un)
		self.failUnlessEqual(card1, card2)


	def test_01b(self):
		" compare deux cartes de valeurs identiques mais de bois different"

		card1 = createCard(Trefle, Un)
		card2 = createCard(Carreau, Un)
		self.failIfEqual(card1, card2)


	def test_01c(self):
		" compare deux cartes de valeurs differentes mais de bois idndentique"
		card1 = createCard(Trefle, Un)
		card2 = createCard(Trefle, Deux)
		self.failIfEqual(card1, card2)


	def test_02a(self):
		" compare la force de deux cartes de valeurs differentes mais de bois idndentique"
		card2 = createCard(Trefle, Deux)
		card1 = createCard(Trefle, Un)
		self.failUnless(card1 < card2)


if __name__ == "__main__":
	unittest.main()


