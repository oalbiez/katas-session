import unittest

from game import required_suit, select_card, select_suit

from card import createCard
from card import Atout, Coeur, Carreau, Trefle, Pique, Excuse
from card import Un, Deux


class CardTestCase(unittest.TestCase):
	def test_01(self):
		self.failIfEqual(Atout("2"), Coeur("2"))
		self.failUnlessEqual(Pique("Roi"), Pique("Roi"))
		self.failIfEqual(Pique("Dame"), Pique("Roi"))


class TarrotTestCase(unittest.TestCase):
	def setUp(self):
		self.hand = [
			createCard(Atout, Un),
			createCard(Atout, Deux),
			Atout("7"),
			Atout("15"),
			Atout("19"),
			Atout("20"),
			Trefle("5"),
			Trefle("8"),
			Trefle("10"),
			Trefle("Roi"),
			Pique("7"),
			Pique("9"),
			Pique("10"),
			Pique("Valet"),
			Pique("Cavalier"),
			Pique("Dame"),
			Pique("Roi"),
			Carreau("2"),
		]


	def test_01(self):
		card=select_card(self.hand, [])
		self.failUnless(card in self.hand)


	def test_02(self):
		cards = select_suit(self.hand, Pique)
		self.failUnlessEqual(cards, (
			Pique("7"),
			Pique("9"),
			Pique("10"),
			Pique("Valet"),
			Pique("Cavalier"),
			Pique("Dame"),
			Pique("Roi"),
		))


	def test_03a(self):
		card=select_card(self.hand, [Carreau("10"),])
		self.failUnlessEqual(card, Carreau("2"))


	def test_03b(self):
		card=select_card(self.hand, [Coeur("10"),])
		self.failUnless(isinstance(card, Atout))


	def test_03c(self):
		card=select_card([
			Carreau("2"),
		], [Coeur("10"),])
		self.failUnlessEqual(card, Carreau("2"))


	def test_04a(self):
		card=select_card(self.hand, [Excuse(""),])
		self.failUnless(card in self.hand)


	def test_05(self):
		"check the required suit from the board"
		board = [Excuse(""), Carreau("1")]
		suit=required_suit(board)
		self.failUnlessEqual(suit, Carreau)



if __name__ == "__main__":
	unittest.main()


