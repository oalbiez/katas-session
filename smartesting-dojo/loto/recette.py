from loto import match


import unittest


class LotoRecette(unittest.TestCase):
	def setUp(self):
		self.tirage = ((17, 16, 22, 13, 40, 45), 1)
		self.joue = {
			"patrice": [16, 17, 22, 13, 40, 45],
			"nadine": [3, 14, 29, 33, 46, 1],
			"nico": [22, 16, 12, 24, 48, 17],
			"steph": [17, 22, 40, 13, 4, 5],
		}

	def test_01(self):
		result = match(self.tirage, self.joue),
		#result dictionnaire nom : (numeros, gain)

		self.failUnlessEqual(result, {
			"patrice": (6, 0.30),
			"nico": (3, 0.075),
			"steph": (3, 0.075),
		})






if __name__ == "__main__":
	unittest.main()


