import loto
import unittest


class LotoTestCase(unittest.TestCase):

	def setUp(self):
		self.tirage = ((17, 16, 22, 13, 40, 45), 1)

	def test_01(self):
		joueur = [16, 17, 22, 13, 40, 45]
		rang = loto.getRang(self.tirage, joueur)
		self.failUnlessEqual(rang, 1)

	def test_05(self):
		joueur = [1, 16, 22, 13, 40, 45]
		rang = loto.getRang(self.tirage, joueur)
		self.failUnlessEqual(rang, 2)

	def test_02(self):
		joueur = [16, 17, 22, 13, 40, 30]
		rang = loto.getRang(self.tirage, joueur)
		self.failUnlessEqual(rang, 3)

	def test_02a(self):
		joueur = [16, 17, 22, 13, 20, 1]
		rang = loto.getRang(self.tirage, joueur)
		self.failUnlessEqual(rang, 4)

	def test_03(self):
		joueur = [16, 17, 22, 13, 20, 30]
		rang = loto.getRang(self.tirage, joueur)
		self.failUnlessEqual(rang, 5)


	def test_04(self):
		joueur = [16, 17, 22, 23, 20, 30]
		rang = loto.getRang(self.tirage, joueur)
		self.failUnlessEqual(rang, 6)

	def test_05(self):
		joueur = [16, 17, 22, 1, 20, 30]
		rang = loto.getRang(self.tirage, joueur)
		self.failUnlessEqual(rang, 6)

	def test_05(self):
		joueur = [2, 3, 4, 5, 6, 7]
		rang = loto.getRang(self.tirage, joueur)
		self.failUnlessEqual(rang, 0)


if __name__ == "__main__":
	unittest.main()


